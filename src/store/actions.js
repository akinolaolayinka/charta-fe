import {login} from "../util/UserHandler";
import {keyBy} from "lodash";
import Toast from "../util/Toast";
import {createProduct, fetchProducts, updateProduct} from "../util/ProductsHandler";
import {createSale, fetchSales} from "../util/SalesHandler";

export const actions = {

  async login(context, payload) {
    try {
      let response = await login(payload);
      Toast.SUCCESS(response.message, 2000);
      context.commit('SAVE_TOKEN', response.data.token);
    } catch (e) {
      Toast.ERROR(e.error);
      return Promise.reject(e);
    }
  },

  register(context, payload) {

  },


  /*Sales*/
  async fetchSales(context, payload) {
    try {
      let response = await fetchSales();
      let sales = keyBy(response.data, 'id');
      context.commit('FETCH_SALES', sales);
      return null;
    } catch (e) {
      console.log(e);
    }
  },

  async createSale(context, payload) {
    try {
      let response = await createSale(payload);
      context.commit('ADD_SALE', response.data);
      Toast.SUCCESS(response.message, 2000);
    } catch (e) {
      console.log(e);
    }
  },




  /*Products*/
  async fetchProducts(context) {
    try {
      let response = await fetchProducts();
      let products = keyBy(response.data, 'id');
      context.commit('FETCH_PRODUCTS', products);
      return null;
    } catch (e) {
      console.log(e);
    }
  },

  async createProduct(context, payload) {
    try {
      let response = await createProduct(payload);
      context.commit('ADD_PRODUCT', response.data);
      Toast.SUCCESS(response.message, 2000);
    } catch (e) {
      console.log(e);
    }
  },

  async updateProduct(context, payload) {
    try {
      let response = await updateProduct(payload);
      context.commit('ADD_PRODUCT', response.data);
      Toast.SUCCESS(response.message, 2000);
    } catch (e) {
      console.log(e);
    }
  },
};
