import Vue from "vue";
export const mutations = {

  SAVE_TOKEN(state, payload) {
    Vue.set(state, 'token', payload);
  },


  FETCH_PRODUCTS(state, payload) {
    Vue.set(state, 'products', payload);
  },

  ADD_PRODUCT(state, payload) {
    Vue.set(state.products, payload.id, payload);
  },


  FETCH_SALES(state, payload) {
    Vue.set(state, 'sales', payload);
  },

  ADD_SALE(state, payload) {
    Vue.set(state.sales, payload.id, payload);
  },

};
