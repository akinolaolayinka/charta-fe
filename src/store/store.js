import Vue from 'vue'
import Vuex from 'vuex'
import {actions} from "./actions";
import {getters} from "./getters";
import {state} from "./state";
import {mutations} from "./mutations";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  plugins : [
    createPersistedState({
      key: 'charta'
    })
  ]
})
