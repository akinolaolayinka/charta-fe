import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/auth/LoginContainer'
import Dashboard from '@/components/dashboard/DashboardContainer'
import Products from '@/components/products/ProductsContainer'
import Sales from '@/components/sales/SalesContainer'

Router.mode = 'history';
Vue.use(Router);

export default new Router({
  mode : 'history',
  routes : [
    {
      path : '/',
      component : HelloWorld,
      children : [
        {path : '/', name : 'Dashboard', component : Dashboard},
        {path : '/products', name : 'Products', component : Products},
        {path : '/sales', name : 'Sales', component : Sales},
      ]
    },
    {path : '/login', name : 'Login', component : Login}
  ]
})
