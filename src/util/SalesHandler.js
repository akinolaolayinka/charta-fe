import {HTTP_BEARER} from "./http-commons";

export const fetchSales = async () => {
  try {
    let response = await HTTP_BEARER().get('/api/v1/sales');
    return Promise.resolve(response.data);
  } catch (e) {
    return Promise.reject({error: e.response.data.message});
  }
};

export const createSale = async (data) => {
  try {
    let response = await HTTP_BEARER().post('/api/v1/sales', data);
    return Promise.resolve(response.data);
  } catch (e) {
    return Promise.reject({error: e.response.data.message});
  }
};
