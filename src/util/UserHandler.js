import {HTTP} from "./http-commons";

export const login = async (data) => {
  try {
    let response = await HTTP.post('/api/v1/auth/login', data);
    return Promise.resolve(response.data);
  } catch (e) {
    return Promise.reject({error: e.response.data.message});
  }
};
