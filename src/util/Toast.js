import {Message} from 'element-ui';
const defaultDuration = 7000;
export default {
  Show : function (message = 'Error!', duration = defaultDuration, style = '') {
    //return M.toast({html : message, displayLength : duration, classes : style})
  },

  SUCCESS : function (message = 'Success!', duration = defaultDuration, style = '') {
    Message({
      message,
      type: 'success',
      duration,
      customClass: 'toast'
    });
  },

  ERROR : function (message = 'Error!', duration = defaultDuration, style = 'rounded') {
    Message({
      message,
      type: 'error',
      duration
    });
  },

  INFO : function (message = 'Error!', duration = defaultDuration, style = 'rounded') {
    Message({
      message,
      type: 'info',
      duration
    });
  }

}
