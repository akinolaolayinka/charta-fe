import {HTTP_BEARER} from "./http-commons";

export const fetchProducts = async () => {
  try {
    let response = await HTTP_BEARER().get('/api/v1/products');
    return Promise.resolve(response.data);
  } catch (e) {
    return Promise.reject({error: e.response.data.message});
  }
};

export const createProduct = async (data) => {
  try {
    let response = await HTTP_BEARER().post('/api/v1/products', data);
    return Promise.resolve(response.data);
  } catch (e) {
    return Promise.reject({error: e.response.data.message});
  }
};


export const updateProduct = async (data) => {
  try {
    let response = await HTTP_BEARER().put(`/api/v1/products/${data.id}`, data);
    return Promise.resolve(response.data);
  } catch (e) {
    return Promise.reject({error: e.response.data.message});
  }
};
