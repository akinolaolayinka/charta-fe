import {sumBy} from 'lodash';

export const evaluateSalesAmount = (sales)=>{
  return {
    totalProfit: sumBy(sales, sale=>{
      return Number(sale.profit);
    }),
    totalSales: sumBy(sales, sale=>{
      return Number(sale.price);
    })
  };
};
