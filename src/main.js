// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store/store'
import Element from 'element-ui';
import 'material-colors/dist/colors.css';
import 'element-ui/lib/theme-chalk/display.css';
import locale from 'element-ui/lib/locale/lang/en';
import VueMq from 'vue-mq';

Vue.use(Element,{locale});
Vue.prototype.$message = Element.Message;

Vue.use(VueMq, {
  breakpoints : {
    mobile : 450,
    phablet : 640,
    tablet : 900,
    laptop : 1250,
    desktop : Infinity,
  }
});

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
